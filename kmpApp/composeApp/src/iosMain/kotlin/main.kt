import androidx.compose.ui.window.ComposeUIViewController
import com.talktor.App
import platform.UIKit.UIViewController

fun MainViewController(): UIViewController = ComposeUIViewController { App() }
