package com.talktor.presentation.core.loading

sealed class LoadingStatus {
    data object Loading : LoadingStatus()
    class Error(val message: String): LoadingStatus()
    data object Idle : LoadingStatus()

}