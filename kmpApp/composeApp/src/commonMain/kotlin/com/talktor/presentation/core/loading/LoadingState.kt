package com.talktor.presentation.core.loading

import kotlinx.coroutines.flow.StateFlow

interface LoadingState {
    val state: StateFlow<LoadingStatus>

    fun loading()
    fun idle()
    fun error(value: String)
}