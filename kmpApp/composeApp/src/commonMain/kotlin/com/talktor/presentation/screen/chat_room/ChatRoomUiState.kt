package com.talktor.presentation.screen.chat_room

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.talktor.domain.model.ChatRoom

data class ChatRoomUiState(
    val chatRooms: List<ChatRoom> = emptyList(),
    val connected: Boolean = false,
    val newChatRoom: MutableState<String> = mutableStateOf("")
)
