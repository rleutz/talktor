package com.talktor.presentation.theme

import androidx.compose.ui.unit.sp

object Font {
    val min = 8.sp
    val small = 12.sp
    val medium = 16.sp
    val large = 22.sp
    val big = 32.sp
}