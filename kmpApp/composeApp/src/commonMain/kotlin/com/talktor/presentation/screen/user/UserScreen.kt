package com.talktor.presentation.screen.user

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import com.talktor.Res
import com.talktor.presentation.component.button.ButtonComponent
import com.talktor.presentation.component.layout.ColumnCenteredComponent
import com.talktor.presentation.component.text.InputTextComponent
import com.talktor.presentation.screen.chat_room.ChatRoomScreen
import com.talktor.presentation.theme.Dimension
import com.talktor.presentation.theme.aux
import com.talktor.presentation.widget.save_username.SaveUsernameWidget

class UserScreen : Screen {

    private val stateHolder = UserStateHolder()

    @Composable
    override fun Content() {
        val uiSTate by stateHolder.uiState.collectAsState()
        val navigator = LocalNavigator.current
        val username = remember {
            uiSTate.username
        }
        val save = remember {
            uiSTate.save
        }
        ColumnCenteredComponent(
            modifier = Modifier.background(aux).padding(Dimension.medium)
        ) {
            InputTextComponent(
                label = Res.string.username,
                value = username
            )
            Spacer(modifier = Modifier.height(Dimension.medium))
            SaveUsernameWidget(save = save)
            Spacer(modifier = Modifier.height(Dimension.large))
            ButtonComponent(
                title = Res.string.enter
            ) {
                if (uiSTate.save.value) stateHolder.save()
                navigator?.replace(ChatRoomScreen(uiSTate.username.value))
            }
        }
    }
}