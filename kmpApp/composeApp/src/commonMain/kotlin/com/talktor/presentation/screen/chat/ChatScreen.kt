package com.talktor.presentation.screen.chat

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import com.talktor.Res
import com.talktor.domain.core.DataType
import com.talktor.domain.model.Chat
import com.talktor.presentation.component.text.InputMessageComponent
import com.talktor.presentation.core.MainScreen
import com.talktor.presentation.screen.chat_room.ChatRoomScreen
import com.talktor.presentation.widget.chat.ChatTextWidget
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json

class ChatScreen(
    private val username: String,
    private val chatRoomName: String
) : Screen {

    private val stateHolder = ChatStateHolder(chatRoomName)

    @Composable
    override fun Content() {
        val scope = rememberCoroutineScope()
        val navigator = LocalNavigator.current
        val uiState by stateHolder.uiState.collectAsState()
        val message = remember(uiState.message) {
            uiState.message
        }
        val chatListState = rememberLazyListState()
        DisposableEffect(true) {
            onDispose {
                scope.launch {
                    stateHolder.exit()
                }
            }
        }
        LaunchedEffect(uiState.messages) {
            chatListState.animateScrollToItem(chatListState.layoutInfo.totalItemsCount)
        }
        MainScreen { mainApp ->
            LaunchedEffect(true) {
                mainApp.topBar.clearActions()
                mainApp.topBar.setBackButton(true)
                mainApp.topBar.setBackButtonExtra {
                    scope.launch {
                        stateHolder.exit()
                        navigator?.replace(ChatRoomScreen(username))
                    }
                }
                mainApp.topBar.setTitle(chatRoomName)
            }
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                LazyColumn(
                    modifier = Modifier.fillMaxWidth().weight(1f),
                    state = chatListState
                ) {
                    items(uiState.messages) { chat ->
                        var hasTriangle = true
                        var hasName = true
                        val index = uiState.messages.indexOf(chat) + 1
                        val indexName = uiState.messages.indexOf(chat) - 1
                        val bol = if (chat.dataType == DataType.CHAT) {
                            val data = Json.decodeFromString<Chat>(chat.data)
                            if (index != uiState.messages.size)
                                hasTriangle = if (uiState.messages[index].dataType == DataType.CHAT)
                                    Json.decodeFromString<Chat>(
                                        uiState.messages[index].data
                                    ).userName != data.userName else false
                            if(indexName >= 0)
                                hasName = if(uiState.messages[indexName].dataType == DataType.CHAT)
                                    Json.decodeFromString<Chat>(
                                        uiState.messages[indexName].data
                                    ).userName != data.userName else true
                            data.userName == username
                        } else false
                        ChatTextWidget(
                            chat,
                            bol,
                            hasTriangle,
                            hasName
                        )
                    }
                }
                InputMessageComponent(
                    label = Res.string.message,
                    value = message
                ) {
                    scope.launch {
                        stateHolder.send()
                    }
                }
            }
        }
    }
}