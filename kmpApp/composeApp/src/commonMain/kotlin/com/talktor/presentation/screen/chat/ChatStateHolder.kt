package com.talktor.presentation.screen.chat

import androidx.compose.runtime.mutableStateOf
import com.talktor.domain.core.DataType
import com.talktor.domain.core.ResourceType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.presentation.core.loading.LoadingState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ChatStateHolder(
    private val chatRoomName: String
) : KoinComponent {

    private val useCases by inject<ChatUseCases>()
    private val chatRoomUseCases by inject<ChatRoomUseCases>()
    private val loading by inject<LoadingState>()

    private val _uiState: MutableStateFlow<ChatUiState> = MutableStateFlow(ChatUiState())
    val uiState: StateFlow<ChatUiState> = _uiState.asStateFlow()

    private val scope = CoroutineScope(Dispatchers.Default)
    private val flowState = scope.launch {
        useCases.getMessages.invoke().cancellable().collectLatest { chat ->
            val messageList = uiState.value.messages.toMutableList()
            messageList.add(chat)
            _uiState.update { currentState ->
                currentState.copy(
                    messages = messageList
                )
            }
        }
    }

    init {
        val scope = CoroutineScope(Dispatchers.Default)
        scope.launch {
            useCases.join.invoke(chatRoomName)
            val chatRoom =
                chatRoomUseCases.getInfo.invoke(chatRoomName)
                    ?: kotlin.run {
                        loading.error("No chat room")
                        return@launch
                    }
            _uiState.update { currentState ->
                currentState.copy(
                    chatRoom = mutableStateOf(chatRoom)
                )
            }
            val oldMessages =
                useCases.getOldMessages.invoke(uiState.value.chatRoom.value?.name.toString())
            val resourceList = mutableListOf<SocketResourceDTO>()
            oldMessages.forEach {
                resourceList.add(
                    SocketResourceDTO(
                        type = ResourceType.RESPONSE,
                        dataType = DataType.CHAT,
                        data = Json.encodeToString(it)
                    )
                )
                _uiState.update { currentState ->
                    currentState.copy(
                        messages = resourceList
                    )
                }
            }
        }
    }

    suspend fun exit() {
        flowState.cancel()
        useCases.exit.invoke()
    }

    suspend fun send() {
        useCases.sendMessage.invoke(uiState.value.message.value)
        _uiState.update { currentState ->
            currentState.copy(
                message = mutableStateOf("")
            )
        }
    }
}