package com.talktor.presentation.component.text

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import com.talktor.presentation.theme.primary
import com.talktor.presentation.theme.secondary

@Composable
fun InputRoomNameComponent(
    modifier: Modifier = Modifier,
    label: String,
    value: MutableState<String>,
    onLeadingClick: () -> Unit,
    onTrailClick: () -> Unit
) {
    TextField(
        modifier = modifier.fillMaxWidth(),
        label = {
            Text(text = label)
        },
        value = value.value,
        onValueChange = {
            value.value = it
        },
        trailingIcon = {
            IconButton(
                onClick = onTrailClick
            ) {
                Icon(Icons.Default.Send, null)
            }
        },
        leadingIcon = {
            IconButton(
                onClick = onLeadingClick
            ) {
                Icon(Icons.Default.Close, null)
            }
        },
        colors = TextFieldDefaults.colors(
            cursorColor = primary,
            focusedLeadingIconColor = primary,
            unfocusedLeadingIconColor = primary,
            focusedTrailingIconColor = primary,
            focusedLabelColor = primary,
            unfocusedLabelColor = primary,
            focusedPlaceholderColor = primary,
            unfocusedPlaceholderColor = primary,
            focusedIndicatorColor = primary,
            focusedSuffixColor = primary,
            focusedContainerColor = secondary,
            unfocusedContainerColor = secondary,
            focusedTextColor = primary,
            unfocusedTextColor = primary
        )
    )
}