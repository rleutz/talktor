package com.talktor.presentation.util

import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

object DateUtil {
    fun toTime(date: Long): String {
        val instant = Instant.fromEpochMilliseconds(date)
        val data = instant.toLocalDateTime(TimeZone.currentSystemDefault())
        val hour = if (data.hour.toString().length == 1) "0${data.hour}" else data.hour.toString()
        val minute =
            if (data.minute.toString().length == 1) "0${data.minute}" else data.minute.toString()
        return "$hour:$minute"
    }
}