package com.talktor.presentation.core.top_bar

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import com.talktor.Res

class TopBarStateImpl: TopBarState {

    private val _actions: MutableState<@Composable () -> Unit> = mutableStateOf({})
    override val actions: State<@Composable () -> Unit>
        get() = _actions
    private val _isBackButton: MutableState<Boolean> = mutableStateOf(false)
    override val isBackButton: State<Boolean> = _isBackButton

    private val _title: MutableState<String> = mutableStateOf(Res.string.app_name)
    override val title: State<String> = _title

    private val _backButtonExtra: MutableState<() -> Unit> = mutableStateOf({})
    override val backButtonExtra: State<() -> Unit>
        get() = _backButtonExtra


    override fun clearActions() {
        _actions.value = {}
    }

    override fun setActions(value: @Composable () -> Unit) {
        _actions.value = value
    }

    override fun setBackButton(value: Boolean) {
        _isBackButton.value = value
    }

    override fun setBackButtonExtra(value: () -> Unit) {
        _backButtonExtra.value = value
    }

    override fun setTitle(value: String) {
        _title.value = value
    }

}