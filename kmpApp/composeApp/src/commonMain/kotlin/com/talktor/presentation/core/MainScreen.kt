package com.talktor.presentation.core

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import com.talktor.presentation.component.layout.ColumnCenteredComponent
import com.talktor.presentation.core.loading.LoadingStatus
import com.talktor.presentation.theme.aux
import com.talktor.presentation.widget.top_bar.TopBarWidget

@Composable
fun MainScreen(
    content: @Composable (MainApp) -> Unit
) {
    val mainApp = rememberMainApp()
    val loadingStatus = mainApp.loading.state.collectAsState()
    Scaffold(
        modifier =
        Modifier.fillMaxSize(),
        topBar = {
            TopBarWidget(mainApp)
        },
        containerColor = aux
    ) { innerPadding ->
        Box(modifier = Modifier.fillMaxSize().padding(innerPadding)) {
            when (loadingStatus.value) {
                is LoadingStatus.Loading -> {
                    ColumnCenteredComponent {
                        CircularProgressIndicator()
                    }
                }
                is LoadingStatus.Idle -> {
                    content(mainApp)
                }
                is LoadingStatus.Error -> {

                }
            }
        }
    }
}