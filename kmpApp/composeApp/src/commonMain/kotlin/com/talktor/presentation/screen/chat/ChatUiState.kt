package com.talktor.presentation.screen.chat

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.model.ChatRoom

data class ChatUiState(
    val chatRoom: MutableState<ChatRoom?> = mutableStateOf(null),
    val messages: List<SocketResourceDTO> = emptyList(),
    val message: MutableState<String> = mutableStateOf("")
)
