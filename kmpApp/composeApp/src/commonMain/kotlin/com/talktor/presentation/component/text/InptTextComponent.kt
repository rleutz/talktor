package com.talktor.presentation.component.text

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import com.talktor.presentation.theme.aux
import com.talktor.presentation.theme.secondary

@Composable
fun InputTextComponent(
    modifier: Modifier = Modifier,
    label: String,
    value: MutableState<String>
) {
    OutlinedTextField(
        modifier = modifier.fillMaxWidth(),
        label = {
            Text(text = label)
        },
        value = value.value,
        onValueChange = {
            value.value = it
        },
        colors = TextFieldDefaults.colors(
            cursorColor = secondary,
            focusedTrailingIconColor = secondary,
            focusedLabelColor = secondary,
            unfocusedLabelColor = secondary,
            focusedPlaceholderColor = secondary,
            unfocusedPlaceholderColor = secondary,
            focusedIndicatorColor = secondary,
            focusedSuffixColor = secondary,
            focusedContainerColor = aux,
            unfocusedContainerColor = aux,
            focusedTextColor = secondary,
            unfocusedTextColor = secondary
        )
    )
}