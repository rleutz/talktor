package com.talktor.presentation.screen.chat_room

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import com.talktor.Res
import com.talktor.presentation.component.layout.ColumnCenteredComponent
import com.talktor.presentation.component.text.InputRoomNameComponent
import com.talktor.presentation.core.MainScreen
import com.talktor.presentation.screen.chat.ChatScreen
import com.talktor.presentation.theme.Dimension
import com.talktor.presentation.theme.Font
import com.talktor.presentation.theme.aux
import com.talktor.presentation.theme.primary
import com.talktor.presentation.theme.secondary
import compose.icons.FeatherIcons
import compose.icons.feathericons.RefreshCw
import kotlinx.coroutines.launch

class ChatRoomScreen(
    private val username: String
) : Screen {

    private val stateHolder = ChatRoomStateHolder(username)

    @Composable
    override fun Content() {
        val uiState by stateHolder.uiState.collectAsState()
        val scope = rememberCoroutineScope()
        val navigator = LocalNavigator.current
        val newRoom = remember {
            mutableStateOf(false)
        }
        val fabVisibility = remember {
            mutableStateOf(true)
        }
        MainScreen { mainApp ->
            LaunchedEffect(true) {
                stateHolder.getAllChatRooms()
                mainApp.topBar.setBackButton(false)
                mainApp.topBar.setTitle(Res.string.chat_room_list)
                mainApp.topBar.setActions {
                    IconButton(
                        onClick = {
                            scope.launch {
                                stateHolder.getAllChatRooms()
                            }
                        }
                    ) {
                        Icon(imageVector = FeatherIcons.RefreshCw, null)
                    }
                }
            }
            Box(modifier = Modifier.fillMaxSize()) {
                Column(modifier = Modifier.fillMaxSize()) {
                    if (uiState.chatRooms.isEmpty()) {
                        ColumnCenteredComponent(modifier = Modifier.fillMaxWidth().weight(1f)) {
                            Text(
                                text = Res.string.empty_list,
                                fontSize = Font.large,
                                color = secondary
                            )
                        }
                    } else {
                        LazyColumn(modifier = Modifier.fillMaxWidth().weight(1f)) {
                            items(uiState.chatRooms) { chatRoom ->
                                TextButton(
                                    modifier = Modifier.fillMaxWidth(),
                                    onClick = {
                                        if (chatRoom.id != null) navigator?.push(
                                            ChatScreen(
                                                username,
                                                chatRoom.name
                                            )
                                        )
                                    },
                                    colors = ButtonDefaults.textButtonColors(
                                        containerColor = aux,
                                        contentColor = secondary
                                    )
                                ) {
                                    Text(chatRoom.name)
                                }
                                Divider(
                                    modifier = Modifier.fillMaxWidth().height(Dimension.minimal)
                                )
                            }
                        }
                    }
                    AnimatedVisibility(newRoom.value) {
                        val chatRoomName = remember {
                            uiState.newChatRoom
                        }
                        InputRoomNameComponent(
                            value = chatRoomName,
                            label = Res.string.new_chat_room,
                            onLeadingClick = {
                                newRoom.value = false
                                fabVisibility.value = true
                            }
                        ) {
                            scope.launch {
                                if (uiState.newChatRoom.value.isNotBlank())
                                    navigator?.push(
                                        ChatScreen(
                                            username, uiState.newChatRoom.value
                                        )
                                    )
                            }
                        }
                    }
                }
                AnimatedVisibility(
                    fabVisibility.value,
                    modifier = Modifier.align(Alignment.BottomEnd)
                ) {
                    FloatingActionButton(
                        modifier = Modifier.align(Alignment.BottomEnd).padding(Dimension.large),
                        onClick = {
                            fabVisibility.value = false
                            newRoom.value = true
                        },
                        containerColor = secondary,
                        contentColor = primary
                    ) {
                        Icon(Icons.Default.Add, null)
                    }
                }
            }
        }
    }
}