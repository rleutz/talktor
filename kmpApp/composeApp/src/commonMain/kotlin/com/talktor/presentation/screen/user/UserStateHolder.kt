package com.talktor.presentation.screen.user

import co.touchlab.kermit.Logger
import com.talktor.domain.use_case.username.UsernameUseCases
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class UserStateHolder: KoinComponent {

    private val useCases by inject<UsernameUseCases>()

    private val _uiState: MutableStateFlow<UserUiState> = MutableStateFlow(UserUiState())
    val uiState: StateFlow<UserUiState> = _uiState.asStateFlow()

    fun save() {
        useCases.setSave.invoke(true)
        useCases.set.invoke(uiState.value.username.value)
    }

    fun enter() {
        Logger.e(uiState.value.username.value)
    }
}