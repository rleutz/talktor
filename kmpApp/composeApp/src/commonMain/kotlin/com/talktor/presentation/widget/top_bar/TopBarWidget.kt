package com.talktor.presentation.widget.top_bar

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIosNew
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import com.talktor.presentation.core.MainApp
import com.talktor.presentation.theme.primary
import com.talktor.presentation.theme.secondary

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBarWidget(
    mainApp: MainApp
) {
    val actions by remember {
        mainApp.topBar.actions
    }
    val title by remember {
        mainApp.topBar.title
    }
    val isBackButton by remember {
        mainApp.topBar.isBackButton
    }
    TopAppBar(
        title = {
            Text(title)
        },
        navigationIcon = {
            IconButton(
                onClick = {
                    if (isBackButton) {
                        mainApp.topBar.backButtonExtra.value()
                        mainApp.topBar.setBackButtonExtra {}
                    }
                }
            ) {
                Icon(
                    imageVector = if (isBackButton) Icons.Default.ArrowBackIosNew else Icons.Default.Menu,
                    contentDescription = null
                )
            }
        },
        actions = {
            actions()
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = primary,
            titleContentColor = secondary,
            navigationIconContentColor = secondary,
            actionIconContentColor = secondary
        )
    )
}