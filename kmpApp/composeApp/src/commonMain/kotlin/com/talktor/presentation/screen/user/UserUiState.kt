package com.talktor.presentation.screen.user

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

data class UserUiState(
    val username: MutableState<String> = mutableStateOf(""),
    val save: MutableState<Boolean> = mutableStateOf(false)
)
