package com.talktor.presentation.component.button

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.talktor.presentation.theme.Dimension
import com.talktor.presentation.theme.primary
import com.talktor.presentation.theme.secondary

@Composable
fun ButtonComponent(
    modifier: Modifier = Modifier,
    title: String,
    onClick: () -> Unit
) {
    Button(
        modifier = modifier.fillMaxWidth(),
        onClick = {
            onClick()
        },
        colors = ButtonDefaults.buttonColors(
            containerColor = secondary,
            contentColor = primary
        ),
        elevation = ButtonDefaults.buttonElevation(
            defaultElevation = Dimension.large,
            pressedElevation = Dimension.none
        )
    ) {
        Text(title)
    }
}