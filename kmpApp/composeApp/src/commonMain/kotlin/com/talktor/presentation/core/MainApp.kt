package com.talktor.presentation.core

import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import com.talktor.presentation.core.loading.LoadingState
import com.talktor.presentation.core.top_bar.TopBarState
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MainApp(
    val drawerState: DrawerState
) : KoinComponent {
    val topBar by inject<TopBarState>()
    val loading by inject<LoadingState>()
}

@Composable
fun rememberMainApp(
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
): MainApp {
    return MainApp(
        drawerState = drawerState
    )
}