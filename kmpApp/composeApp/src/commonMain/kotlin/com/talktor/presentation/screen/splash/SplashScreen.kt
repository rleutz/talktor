package com.talktor.presentation.screen.splash

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import com.talktor.presentation.component.layout.ColumnCenteredComponent
import com.talktor.presentation.screen.chat_room.ChatRoomScreen
import com.talktor.presentation.screen.user.UserScreen

class SplashScreen: Screen {

    private val stateHolder = SplashStateHolder()
    @Composable
    override fun Content() {
        val navigator = LocalNavigator.current
        LaunchedEffect(true) {
            stateHolder.autoLogin { b, s ->
                if(b && s != null) {
                    navigator?.replace(ChatRoomScreen(s))
                } else {
                    navigator?.replace(UserScreen())
                }
            }
        }
        ColumnCenteredComponent {
            Text("Splash")
        }
    }
}