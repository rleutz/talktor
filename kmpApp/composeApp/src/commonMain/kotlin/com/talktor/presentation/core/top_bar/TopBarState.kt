package com.talktor.presentation.core.top_bar

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State

interface TopBarState {
    val backButtonExtra: State<() -> Unit>
    val isBackButton: State<Boolean>
    val title: State<String>
    val actions: State<@Composable () -> Unit>

    fun clearActions()
    fun setActions(value: @Composable () -> Unit)
    fun setBackButton(value: Boolean)
    fun setBackButtonExtra(value: () -> Unit)
    fun setTitle(value: String)
}