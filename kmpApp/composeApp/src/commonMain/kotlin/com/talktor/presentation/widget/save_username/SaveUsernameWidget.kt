package com.talktor.presentation.widget.save_username

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.talktor.Res
import com.talktor.presentation.theme.primary
import com.talktor.presentation.theme.secondary

@Composable
fun SaveUsernameWidget(
    modifier: Modifier = Modifier,
    save: MutableState<Boolean>
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            checked = save.value,
            onCheckedChange = {
                save.value = !save.value
            },
            colors = CheckboxDefaults.colors(
                checkedColor = secondary,
                checkmarkColor = primary
            )
        )
        Text(Res.string.save_username)
    }
}