package com.talktor.presentation.widget.chat

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import com.talktor.domain.core.DataType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.model.Chat
import com.talktor.domain.model.ChatInfo
import com.talktor.presentation.theme.Dimension
import com.talktor.presentation.theme.Font
import com.talktor.presentation.util.DateUtil
import kotlinx.serialization.json.Json

@Composable
fun ChatTextWidget(
    chat: SocketResourceDTO,
    isUser: Boolean,
    hasTriangle: Boolean = true,
    hasName: Boolean = true
) {
    when (chat.dataType) {
        DataType.CHAT -> {
            val data = Json.decodeFromString<Chat>(chat.data)
            Column(
                modifier = Modifier.fillMaxWidth()
                    .padding(
                        horizontal =
                        if (hasTriangle) Dimension.small
                        else Dimension.medium + Dimension.small,
                    ),
                horizontalAlignment =
                if (isUser) Alignment.End
                else Alignment.Start
            ) {
                Row(
                    verticalAlignment = Alignment.Bottom
                ) {
                    if (!isUser && hasTriangle) {
                        Triangle(
                            color = if (isUser) Color.LightGray else Color.DarkGray,
                            width = Dimension.medium,
                            height = Dimension.medium
                        )
                    }
                    Column(
                        modifier = Modifier.clip(
                            RoundedCornerShape(
                                topStart = Dimension.medium,
                                topEnd = Dimension.medium,
                                bottomStart = if (!isUser && hasTriangle) Dimension.none else Dimension.medium,
                                bottomEnd = if (isUser && hasTriangle) Dimension.none else Dimension.medium
                            )
                        ).background(
                            if (isUser) Color.LightGray
                            else Color.DarkGray
                        ).padding(Dimension.medium)
                    ) {
                        if (!isUser && hasName) {
                            Text(
                                text = data.userName,
                                fontSize = Font.small,
                                color = Color.LightGray,
                                textAlign = if (isUser) TextAlign.End else TextAlign.Start
                            )
                        }
                        Text(
                            text = data.message,
                            fontSize = Font.medium,
                            color = if (isUser) Color.Black else Color.White
                        )
                        Text(
                            modifier = Modifier.align(if (isUser) Alignment.End else Alignment.Start),
                            text = DateUtil.toTime(data.date),
                            fontSize = Font.min,
                            color = if (isUser) Color.DarkGray else Color.LightGray,
                            textAlign = if (isUser) TextAlign.End else TextAlign.Start
                        )
                    }
                    if (isUser && hasTriangle) {
                        Triangle(
                            color = if (isUser) Color.LightGray else Color.DarkGray,
                            width = Dimension.medium,
                            height = Dimension.medium,
                            rotate = 180f
                        )
                    }
                }
                Spacer(modifier = Modifier.height(if (hasTriangle) Dimension.medium else Dimension.small))
            }
        }

        DataType.CHAT_INFO -> {
            val data = Json.decodeFromString<ChatInfo>(chat.data)
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier.clip(RoundedCornerShape(Dimension.medium))
                        .background(Color.DarkGray).padding(Dimension.medium),
                    text = "${data.username} ${data.message}",
                    fontSize = Font.medium,
                    color = Color.White
                )
                Spacer(Modifier.height(Dimension.medium))
            }
        }

        else -> {}
    }
}

@Composable
fun Triangle(color: Color, width: Dp, height: Dp, rotate: Float = 0f) {
    Canvas(
        modifier = Modifier
            .width(width)
            .height(height)
            .graphicsLayer {
                rotationY = rotate
            },
        onDraw = {
            val trianglePath = Path().let {
                it.moveTo(this.size.width, this.size.height)
                it.lineTo(this.size.width, 0f)
                it.lineTo(0f, this.size.height)
                it.close()
                it
            }
            drawPath(trianglePath, color)
        }
    )
}