package com.talktor.presentation.theme

import androidx.compose.ui.unit.dp

object Dimension {
    val none = 0.dp
    val minimal = 1.dp
    val smallest = 2.dp
    val small = 4.dp
    val medium = 8.dp
    val large = 16.dp
    val big = 32.dp
}