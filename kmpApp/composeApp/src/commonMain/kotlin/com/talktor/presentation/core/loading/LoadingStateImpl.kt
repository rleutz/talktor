package com.talktor.presentation.core.loading

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class LoadingStateImpl: LoadingState {

    private val _loadingState: MutableStateFlow<LoadingStatus> = MutableStateFlow(LoadingStatus.Idle)
    override val state: StateFlow<LoadingStatus>
        get() = _loadingState


    override fun loading() {
        _loadingState.value = LoadingStatus.Loading
    }

    override fun idle() {
        _loadingState.value = LoadingStatus.Idle
    }

    override fun error(value: String) {
        _loadingState.value = LoadingStatus.Error(value)
    }
}