package com.talktor.presentation.screen.chat_room

import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.presentation.core.loading.LoadingState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ChatRoomStateHolder(
    private val username: String
): KoinComponent {

    private val useCases by inject<ChatRoomUseCases>()
    private val chatUseCases by inject<ChatUseCases>()
    private val loading by inject<LoadingState>()

    private val _uiState: MutableStateFlow<ChatRoomUiState> = MutableStateFlow(ChatRoomUiState())
    val uiState: StateFlow<ChatRoomUiState> = _uiState.asStateFlow()

    init {
        val scope = CoroutineScope(Dispatchers.Default)
        scope.launch {
            val isConnected = chatUseCases.isConnected.invoke()
            if(!isConnected) chatUseCases.connect.invoke(username)
            _uiState.update { currentState ->
                currentState.copy(
                    connected = isConnected
                )
            }
            getAllChatRooms()
        }
    }

    suspend fun getAllChatRooms() {
        val list = useCases.getAll.invoke()
        _uiState.update { currentState ->
            currentState.copy(
                chatRooms = list
            )
        }
    }

    suspend fun exit() {
        chatUseCases.exit.invoke()
    }

}