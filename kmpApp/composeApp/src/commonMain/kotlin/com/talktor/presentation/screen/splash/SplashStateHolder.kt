package com.talktor.presentation.screen.splash

import com.talktor.domain.use_case.username.UsernameUseCases
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SplashStateHolder: KoinComponent {

    private val useCases by inject<UsernameUseCases>()

    fun autoLogin(callBack: (Boolean, String?) -> Unit) {
        val save = useCases.getSave.invoke()
        if(save) callBack(save, useCases.get.invoke())
        else callBack(false, null)
    }
}