package com.talktor

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.transitions.FadeTransition
import com.talktor.di.dataModule
import com.talktor.di.domainModule
import com.talktor.di.presentationModule
import com.talktor.presentation.screen.splash.SplashScreen
import com.talktor.presentation.theme.AppTheme
import org.koin.core.context.startKoin

@Composable
internal fun App() = AppTheme {
    startKoin {
        modules(
            dataModule,
            domainModule,
            presentationModule
        )
    }
    Navigator(SplashScreen()) {
        FadeTransition(it)
    }
}