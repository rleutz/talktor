package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.model.Chat

class ChatGetOldMessagesUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(chatRoomName: String): List<Chat> =
        repository.getOldMessages(chatRoomName)
}