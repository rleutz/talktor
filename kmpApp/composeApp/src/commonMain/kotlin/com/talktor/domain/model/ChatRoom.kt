package com.talktor.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class ChatRoom(
    val id: String?,
    val name: String
)
