package com.talktor.domain.use_case.username

import com.talktor.data.repository.username.UsernameRepository

class UsernameGetSaveUseCase(
    private val repository: UsernameRepository
) {
    operator fun invoke(): Boolean = repository.getSave()
}