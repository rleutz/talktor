package com.talktor.domain.use_case.chat

data class ChatUseCases(
    val connect: ChatConnectUseCase,
    val exit: ChatExitUseCase,
    val getMessages: ChatGetMessagesUseCase,
    val isConnected: ChatIsConnectedUseCase,
    val join: ChatJoinUseCase,
    val getOldMessages: ChatGetOldMessagesUseCase,
    val sendMessage: ChatSendMessageUseCase
)
