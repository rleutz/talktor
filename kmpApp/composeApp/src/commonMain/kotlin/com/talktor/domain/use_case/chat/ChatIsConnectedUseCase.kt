package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository

class ChatIsConnectedUseCase(
    private val repository: ChatRepository
) {
    operator fun invoke(): Boolean = repository.isConnected()
}