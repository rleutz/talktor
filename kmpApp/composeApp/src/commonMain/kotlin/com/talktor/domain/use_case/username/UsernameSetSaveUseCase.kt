package com.talktor.domain.use_case.username

import com.talktor.data.repository.username.UsernameRepository

class UsernameSetSaveUseCase(
    private val repository: UsernameRepository
) {
    operator fun invoke(value: Boolean) = repository.setSave(value)
}