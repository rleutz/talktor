package com.talktor.domain.use_case.chat_room

import com.talktor.data.repository.chat_room.ChatRoomRepository
import com.talktor.domain.model.ChatRoom

class ChatRoomGetAllUseCase(
    private val repository: ChatRoomRepository
) {
    suspend operator fun invoke(): List<ChatRoom> = repository.getAll()
}