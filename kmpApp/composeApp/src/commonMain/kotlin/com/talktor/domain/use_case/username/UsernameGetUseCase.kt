package com.talktor.domain.use_case.username

import com.talktor.data.repository.username.UsernameRepository

class UsernameGetUseCase(
    private val repository: UsernameRepository
) {
    operator fun invoke(): String = repository.getUsername()
}