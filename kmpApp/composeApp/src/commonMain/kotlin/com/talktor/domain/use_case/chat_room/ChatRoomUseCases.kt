package com.talktor.domain.use_case.chat_room

data class ChatRoomUseCases(
    val getAll: ChatRoomGetAllUseCase,
    val getInfo: ChatRoomGetInfoUseCase
)
