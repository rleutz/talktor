package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.core.DataType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.core.ResourceType

class ChatJoinUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(name: String) {
        val data = SocketResourceDTO(
            type = ResourceType.REQUEST,
            dataType = DataType.JOIN,
            data = name
        )
        repository.send(data)
    }
}