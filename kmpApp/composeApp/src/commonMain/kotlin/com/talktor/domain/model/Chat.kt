package com.talktor.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Chat(
    val id: String?,
    val date: Long,
    val userName: String,
    val chatRoomId: String,
    val message: String
)
