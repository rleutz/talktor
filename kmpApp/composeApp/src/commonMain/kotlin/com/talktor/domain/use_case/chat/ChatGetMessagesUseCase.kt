package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.core.SocketResourceDTO
import kotlinx.coroutines.flow.Flow

class ChatGetMessagesUseCase(
    private val repository: ChatRepository
) {
    operator fun invoke(): Flow<SocketResourceDTO> = repository.getMessages()
}