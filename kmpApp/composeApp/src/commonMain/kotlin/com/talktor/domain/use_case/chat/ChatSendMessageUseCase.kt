package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.core.DataType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.core.ResourceType

class ChatSendMessageUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(message: String) {
        val data = SocketResourceDTO(
            type = ResourceType.REQUEST,
            dataType = DataType.MESSAGE,
            data = message
        )
        repository.send(data)
    }
}