package com.talktor.domain.use_case.username

data class UsernameUseCases(
    val get: UsernameGetUseCase,
    val getSave: UsernameGetSaveUseCase,
    val set: UsernameSetUseCase,
    val setSave: UsernameSetSaveUseCase
)
