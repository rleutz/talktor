package com.talktor.domain.core

import kotlinx.serialization.Serializable

@Serializable
enum class ResourceType {
    REQUEST,
    RESPONSE;
}