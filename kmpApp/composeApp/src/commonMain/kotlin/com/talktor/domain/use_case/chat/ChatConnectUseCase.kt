package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository

class ChatConnectUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(username: String): Boolean = repository.connect(username)
}