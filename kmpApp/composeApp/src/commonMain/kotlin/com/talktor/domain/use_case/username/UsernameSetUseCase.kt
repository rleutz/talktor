package com.talktor.domain.use_case.username

import com.talktor.data.repository.username.UsernameRepository

class UsernameSetUseCase(
    private val repository: UsernameRepository
) {
    operator fun invoke(name: String) = repository.setUsername(name)
}