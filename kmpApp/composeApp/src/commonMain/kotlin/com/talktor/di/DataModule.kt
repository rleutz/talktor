package com.talktor.di

import com.talktor.data.local.data_source.GenericLocalDataSource
import com.talktor.data.local.data_source.GenericLocalDataSourceImpl
import com.talktor.data.remote.api.Api
import com.talktor.data.remote.api.ApiImpl
import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.data.remote.data_source.GenericDataSourceImpl
import com.talktor.data.repository.chat.ChatRepository
import com.talktor.data.repository.chat.ChatRepositoryImpl
import com.talktor.data.repository.chat_room.ChatRoomRepository
import com.talktor.data.repository.chat_room.ChatRoomRepositoryImpl
import com.talktor.data.repository.username.UsernameRepository
import com.talktor.data.repository.username.UsernameRepositoryImpl
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val dataModule = module {
    factoryOf(::GenericLocalDataSourceImpl) bind GenericLocalDataSource::class
    factoryOf(::UsernameRepositoryImpl) bind UsernameRepository::class
    singleOf(::ApiImpl) bind Api::class
    singleOf(::GenericDataSourceImpl) bind GenericDataSource::class
    factoryOf(::ChatRoomRepositoryImpl) bind ChatRoomRepository::class
    factoryOf(::ChatRepositoryImpl) bind ChatRepository::class
}