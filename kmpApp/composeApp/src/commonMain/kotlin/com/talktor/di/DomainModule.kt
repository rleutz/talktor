package com.talktor.di

import com.talktor.domain.use_case.chat.ChatConnectUseCase
import com.talktor.domain.use_case.chat.ChatExitUseCase
import com.talktor.domain.use_case.chat.ChatGetMessagesUseCase
import com.talktor.domain.use_case.chat.ChatGetOldMessagesUseCase
import com.talktor.domain.use_case.chat.ChatIsConnectedUseCase
import com.talktor.domain.use_case.chat.ChatJoinUseCase
import com.talktor.domain.use_case.chat.ChatSendMessageUseCase
import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomGetAllUseCase
import com.talktor.domain.use_case.chat_room.ChatRoomGetInfoUseCase
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.domain.use_case.username.UsernameGetSaveUseCase
import com.talktor.domain.use_case.username.UsernameGetUseCase
import com.talktor.domain.use_case.username.UsernameSetSaveUseCase
import com.talktor.domain.use_case.username.UsernameSetUseCase
import com.talktor.domain.use_case.username.UsernameUseCases
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.module

val domainModule = module {
    factoryOf(::UsernameGetSaveUseCase)
    factoryOf(::UsernameGetUseCase)
    factoryOf(::UsernameSetSaveUseCase)
    factoryOf(::UsernameSetUseCase)
    factoryOf(::UsernameUseCases)

    factoryOf(::ChatRoomGetAllUseCase)
    factoryOf(::ChatRoomGetInfoUseCase)
    factoryOf(::ChatRoomUseCases)

    factoryOf(::ChatConnectUseCase)
    factoryOf(::ChatExitUseCase)
    factoryOf(::ChatGetMessagesUseCase)
    factoryOf(::ChatGetOldMessagesUseCase)
    factoryOf(::ChatIsConnectedUseCase)
    factoryOf(::ChatJoinUseCase)
    factoryOf(::ChatSendMessageUseCase)
    factoryOf(::ChatUseCases)
}