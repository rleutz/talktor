package com.talktor.di

import com.talktor.presentation.core.loading.LoadingState
import com.talktor.presentation.core.loading.LoadingStateImpl
import com.talktor.presentation.core.top_bar.TopBarState
import com.talktor.presentation.core.top_bar.TopBarStateImpl
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val presentationModule = module {
    singleOf(::LoadingStateImpl) bind LoadingState::class
    singleOf(::TopBarStateImpl) bind TopBarState::class
}