package com.talktor.data.repository.chat_room

import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.domain.model.ChatRoom
import io.ktor.client.call.body

class ChatRoomRepositoryImpl(
    private val dataSource: GenericDataSource
): ChatRoomRepository {
    override suspend fun getAll(): List<ChatRoom> = dataSource.get("/chat").body()
    override suspend fun getInfo(name: String): ChatRoom? = dataSource.get("/info/$name").body()
}