package com.talktor.data.remote.data_source

import io.ktor.client.statement.HttpResponse
import io.ktor.websocket.WebSocketSession

interface GenericDataSource {
    var webSocketSession: WebSocketSession?
    suspend fun connectWebSocket(endpoint: String): Boolean
    suspend fun get(endpoint: String): HttpResponse
}