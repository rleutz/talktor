package com.talktor.data.repository.chat

import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.model.Chat
import kotlinx.coroutines.flow.Flow

interface ChatRepository {

    fun isConnected(): Boolean
    suspend fun connect(username: String): Boolean
    fun getMessages(): Flow<SocketResourceDTO>
    suspend fun getOldMessages(chatRoomName: String): List<Chat>
    suspend fun send(data: SocketResourceDTO)
}