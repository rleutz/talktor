package com.talktor.data.local.data_source

interface GenericLocalDataSource {
    fun getBoolean(name: String): Boolean
    fun getString(name: String): String
    fun setBoolean(name: String, value: Boolean)
    fun setString(name: String, value: String)

}