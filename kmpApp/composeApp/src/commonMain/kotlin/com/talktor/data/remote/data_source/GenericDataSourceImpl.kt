package com.talktor.data.remote.data_source

import com.talktor.data.remote.api.Api
import io.ktor.client.plugins.websocket.webSocketSession
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import io.ktor.websocket.WebSocketSession

class GenericDataSourceImpl(
    private val api: Api
) : GenericDataSource {

    companion object {
        private const val SERVER = "192.168.0.198"
    }

    override var webSocketSession: WebSocketSession? = null

    override suspend fun connectWebSocket(endpoint: String): Boolean {
        return try {
            webSocketSession = api.client.webSocketSession(
                method = HttpMethod.Get,
                host = SERVER,
                port = 8080,
                path = endpoint
            )
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun get(endpoint: String): HttpResponse =
        api.client.get("http://$SERVER:8080$endpoint")

}