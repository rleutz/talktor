package com.talktor.data.repository.chat

import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.model.Chat
import io.ktor.client.call.body
import io.ktor.websocket.Frame
import io.ktor.websocket.readText
import io.ktor.websocket.send
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ChatRepositoryImpl(
    private val dataSource: GenericDataSource
) : ChatRepository {

    override fun isConnected(): Boolean = dataSource.webSocketSession != null

    override suspend fun connect(username: String): Boolean =
        dataSource.connectWebSocket("/ws/$username")

    override fun getMessages(): Flow<SocketResourceDTO> {
        return try {
            dataSource.webSocketSession?.incoming?.receiveAsFlow()
                ?.filter { it is Frame.Text }!!
                .map {
                    val message = (it as? Frame.Text)?.readText() ?: ""
                    Json.decodeFromString<SocketResourceDTO>(message)
                }
        } catch (e: Exception) {
            e.printStackTrace()
            flow { }
        }
    }

    override suspend fun getOldMessages(chatRoomName: String): List<Chat> =
        dataSource.get("/chat/$chatRoomName").body<List<Chat>>()

    override suspend fun send(data: SocketResourceDTO) {
        try {
            val json = Json.encodeToString<SocketResourceDTO>(data)
            println(json)
            dataSource.webSocketSession?.send(json)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}