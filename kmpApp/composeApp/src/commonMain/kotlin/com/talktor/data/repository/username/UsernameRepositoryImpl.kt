package com.talktor.data.repository.username

import com.talktor.data.local.data_source.GenericLocalDataSource

class UsernameRepositoryImpl(
    private val dataSource: GenericLocalDataSource
): UsernameRepository {
    override fun getSave(): Boolean = dataSource.getBoolean("save_username")

    override fun getUsername(): String = dataSource.getString("username")

    override fun setSave(value: Boolean) {
        dataSource.setBoolean("save_username", value)
    }

    override fun setUsername(value: String) {
        dataSource.setString("username", value)
    }

}