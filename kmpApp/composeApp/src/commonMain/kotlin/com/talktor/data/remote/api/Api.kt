package com.talktor.data.remote.api

import io.ktor.client.HttpClient

interface Api {
    val client: HttpClient
}