package com.talktor.data.repository.chat_room

import com.talktor.domain.model.ChatRoom

interface ChatRoomRepository {

    suspend fun getAll(): List<ChatRoom>
    suspend fun getInfo(name: String): ChatRoom?
}