package com.talktor.data.repository.username

interface UsernameRepository {
    fun getSave(): Boolean
    fun getUsername(): String
    fun setSave(value: Boolean)
    fun setUsername(value: String)
}