package com.talktor.data.local.data_source

import com.russhwolf.settings.Settings
import com.russhwolf.settings.set

class GenericLocalDataSourceImpl : GenericLocalDataSource {

    private val settings = Settings()
    override fun getBoolean(name: String): Boolean = settings.getBoolean(name, false)
    override fun getString(name: String): String = settings.getString(name, "")
    override fun setBoolean(name: String, value: Boolean) {
        settings[name] = value
    }
    override fun setString(name: String, value: String) {
        settings[name] = value
    }
}