# Talktor - API

### API websocket de chat para database MongoDB

## Endpoints

- `/chat` 
<br>Recebe todas as salas registradas.
- `/chat/[nome da sala]`
<br>Recebe informações da sala.
- `/chat/[nome da sala]`
<br>Recebe as mensagens antigas da sala.
- `/users/[nome da sala]`
<br>Recebe a lista de usuários da sala.
- `/ws/[nome do usuário]`
<br>Conecta no WebSocket.

## Envio de Json no WebSocket

<pre>
{"type":"REQUEST","dataType":"JOIN","data":"Teste"}
{"type":"REQUEST","dataType":"MESSAGE","data":"Olá Mundo!"}
{"type":"REQUEST","dataType":"EXIT","data":""}
</pre>

### Comandos

- JOIN = `Entra em uma sala.`
- EXIT = `Sai da sala.`
- MESSAGE = `Envia uma mensagem.`

## Reposta Json da sala quando recebida mensagem.

<pre>
{
    "type":"RESPONSE",
    "dataType":"CHAT_INFO",
    "data":"{\"chatRoomId\":\"65c5edfeaab8175525b94572\",\"username\":\"Rod\",\"message\":\"entoru na sala.\"}"
}
{
    "type":"RESPONSE",
    "dataType":"CHAT",
    "data":"{\"id\":\"65c612d76a7bd74361efb90c\",\"date\":1707479767515,\"userName\":\"Leutz\",\"chatRoomId\":\"65c5edfeaab8175525b94572\",\"message\":\"Olá Mundo!\"}"}
{
    "type":"RESPONSE",
    "dataType":"CHAT_INFO",
    "data":"{\"chatRoomId\":\"65c5edfeaab8175525b94572\",\"username\":\"Leutz\",\"message\":\"saiu da sala.\"}"
}
</pre>