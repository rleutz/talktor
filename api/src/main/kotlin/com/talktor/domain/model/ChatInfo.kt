package com.talktor.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class ChatInfo(
    val chatRoomId: String,
    val username: String,
    val message: String
)