package com.talktor.domain.core

import kotlinx.serialization.Serializable

@Serializable
data class SocketResourceDTO(
    val type: ResourceType,
    val dataType: DataType,
    val data: String
)