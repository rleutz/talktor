package com.talktor.domain.core

import kotlinx.serialization.Serializable

@Serializable
enum class DataType {
    /**
     * Request
     */
    JOIN,
    EXIT,
    MESSAGE,

    /**
     * Response
     */
    CHAT,
    CHAT_INFO,
    STATUS;
}