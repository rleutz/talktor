package com.talktor.domain.use_case.web_socket

import com.talktor.domain.core.DataType
import com.talktor.domain.core.ResourceType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.model.ChatInfo
import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.presentation.route.chat.WSConnection
import io.ktor.websocket.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class WebSocketReceiveUseCase(
    private val chatRoomUseCases: ChatRoomUseCases,
    private val chatUseCases: ChatUseCases
) {
    suspend operator fun invoke(
        connections: Set<WSConnection>,
        wsConnection: WSConnection,
        resourceDTO: SocketResourceDTO
    ): SocketResourceDTO? {
        return if (resourceDTO.type == ResourceType.REQUEST) {
            when (resourceDTO.dataType) {
                DataType.JOIN -> {
                    val id = chatUseCases.join.invoke(resourceDTO.data)
                    wsConnection.chatRoomId = id
                    if (id == null) wsConnection.session.send("Error create room")
                    SocketResourceDTO(
                        type = ResourceType.RESPONSE,
                        dataType = DataType.CHAT_INFO,
                        data = Json.encodeToString<ChatInfo>(
                            ChatInfo(
                                chatRoomId = wsConnection.chatRoomId.toString(),
                                username = wsConnection.userName,
                                message = "entoru na sala."
                            )
                        )
                    )
                }

                DataType.EXIT -> {
                    val exitMessage = SocketResourceDTO(
                        type = ResourceType.RESPONSE,
                        dataType = DataType.CHAT_INFO,
                        data = Json.encodeToString<ChatInfo>(
                            ChatInfo(
                                chatRoomId = wsConnection.chatRoomId.toString(),
                                username = wsConnection.userName,
                                message = "saiu da sala."
                            )
                        )
                    )
                    connections.filter { it.chatRoomId == wsConnection.chatRoomId }.forEach {
                        it.session.send(Json.encodeToString(exitMessage))
                    }
                    wsConnection.chatRoomId = null
                    exitMessage
                }

                DataType.MESSAGE -> {
                    val chatMessage =
                        chatUseCases.addMessage.invoke(
                            wsConnection.userName,
                            wsConnection.chatRoomId!!,
                            resourceDTO.data
                        ) ?: return null
                    SocketResourceDTO(
                        type = ResourceType.RESPONSE,
                        dataType = DataType.CHAT,
                        data = Json.encodeToString(chatMessage)
                    )
                }

                else -> null
            }
        } else null
    }
}