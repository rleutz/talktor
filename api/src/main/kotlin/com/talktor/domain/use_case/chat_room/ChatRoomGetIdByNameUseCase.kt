package com.talktor.domain.use_case.chat_room

import com.talktor.data.repository.chat.ChatRoomRepository

class ChatRoomGetIdByNameUseCase(
    private val repository: ChatRoomRepository
) {
    suspend operator fun invoke(name: String): String? {
        val chatRoom = repository.getByName(name)?.id
        return chatRoom
    }
}