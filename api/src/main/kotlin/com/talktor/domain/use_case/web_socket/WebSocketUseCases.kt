package com.talktor.domain.use_case.web_socket

data class WebSocketUseCases(
    val receive: WebSocketReceiveUseCase
)
