package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.model.Chat

class ChatGetAllByChatRoomIdUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(chatRoomId: String): List<Chat> {
        val messages = repository.getByChatRoomId(chatRoomId)
        return messages
    }
}