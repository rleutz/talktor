package com.talktor.domain.use_case.chat_room

import com.talktor.data.repository.chat.ChatRoomRepository
import com.talktor.domain.model.ChatRoom

class ChatRoomGetByNameUseCase(
    private val repository: ChatRoomRepository
) {
    suspend operator fun invoke(name: String): ChatRoom? {
        return repository.getByName(name)
    }
}