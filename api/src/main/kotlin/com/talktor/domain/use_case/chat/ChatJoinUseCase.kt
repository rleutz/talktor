package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRoomRepository

class ChatJoinUseCase(
    private val repository: ChatRoomRepository
) {
    suspend operator fun invoke(chatRoomName: String): String? {
        return repository.getByName(chatRoomName)?.id ?: repository.addAndGetId(chatRoomName)
    }
}