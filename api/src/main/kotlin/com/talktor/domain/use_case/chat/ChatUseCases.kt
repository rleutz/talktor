package com.talktor.domain.use_case.chat

data class ChatUseCases(
    val addMessage: ChatAddMessageUseCase,
    val getAllByChatRoomId: ChatGetAllByChatRoomIdUseCase,
    val join: ChatJoinUseCase
)
