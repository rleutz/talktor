package com.talktor.domain.use_case.chat

import com.talktor.data.repository.chat.ChatRepository
import com.talktor.domain.model.Chat

class ChatAddMessageUseCase(
    private val repository: ChatRepository
) {
    suspend operator fun invoke(username: String, chatRoomId: String, message: String): Chat? {
        val chat = Chat(
            userName = username,
            chatRoomId = chatRoomId,
            message = message
        )
        return repository.add(chat)
    }

}