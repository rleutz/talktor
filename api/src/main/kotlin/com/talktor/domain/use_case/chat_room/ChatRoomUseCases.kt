package com.talktor.domain.use_case.chat_room

data class ChatRoomUseCases(
    val getAll: ChatRoomGetAllUseCase,
    val getByName: ChatRoomGetByNameUseCase,
    val getIdByName: ChatRoomGetIdByNameUseCase
)