package com.talktor.data.repository.chat

import com.talktor.data.model.chat.ChatEntity
import com.talktor.data.remote.api.Client
import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.data.remote.data_source.GenericDataSourceImpl
import com.talktor.domain.model.Chat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.bson.types.ObjectId

class ChatRepositoryImpl(
    client: Client
): ChatRepository {

    private val collection = client.database.getCollection<ChatEntity>()
    private val dataSource: GenericDataSource<ChatEntity> = GenericDataSourceImpl(collection)

    init {
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            collection.createIndex("{chatRoomId: 1}")
        }
    }

    override suspend fun add(chat: Chat): Chat? {
        return try {
            val id = dataSource.addAndGetId(ChatEntity.fromChat(chat))
            dataSource.getById(ChatEntity::id, ObjectId(id))?.toChat()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override suspend fun delete(chatId: String): Boolean {
        return try {
            dataSource.delete(ChatEntity::id, chatId)
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getByChatRoomId(chatRoomId: String): List<Chat> {
        return try {
            dataSource.getListByPropertyString(ChatEntity::chatRoomId, chatRoomId).map { it.toChat() }
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }
}