package com.talktor.data.repository.chat

import com.mongodb.client.model.IndexOptions
import com.talktor.data.model.chat.ChatRoomEntity
import com.talktor.data.remote.api.Client
import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.data.remote.data_source.GenericDataSourceImpl
import com.talktor.domain.model.ChatRoom
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatRoomRepositoryImpl(
    private val client: Client
) : ChatRoomRepository {

    private val collection = client.database.getCollection<ChatRoomEntity>()
    private val dataSource: GenericDataSource<ChatRoomEntity> = GenericDataSourceImpl(collection)

    init {
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            collection.createIndex("{name: 1}", IndexOptions().unique(true))
        }
    }

    override suspend fun addAndGetId(name: String): String? =
        dataSource.addAndGetId(ChatRoomEntity.fromChatRoom(ChatRoom(name = name)))

    override suspend fun getAll(): List<ChatRoom> = dataSource.getAll().map { it.toChatRoom() }

    override suspend fun getByName(name: String): ChatRoom? =
        dataSource.getListByPropertyString(ChatRoomEntity::name, name).map { it.toChatRoom() }.firstOrNull()
}