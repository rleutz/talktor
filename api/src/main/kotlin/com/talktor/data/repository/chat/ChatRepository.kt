package com.talktor.data.repository.chat

import com.talktor.domain.model.Chat

interface ChatRepository {
    suspend fun add(chat: Chat): Chat?
    suspend fun delete(chatId: String): Boolean
    suspend fun getByChatRoomId(chatRoomId: String): List<Chat>
}
