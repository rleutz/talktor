package com.talktor.data.repository.chat

import com.talktor.domain.model.ChatRoom

interface ChatRoomRepository {

    suspend fun addAndGetId(name: String): String?
    suspend fun getAll(): List<ChatRoom>
    suspend fun getByName(name: String): ChatRoom?
}