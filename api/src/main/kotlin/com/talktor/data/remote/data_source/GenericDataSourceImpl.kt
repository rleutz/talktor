package com.talktor.data.remote.data_source

import org.bson.BsonObjectId
import org.bson.types.ObjectId
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.eq
import kotlin.reflect.KProperty1

class GenericDataSourceImpl<T: Any>(
    private val collection: CoroutineCollection<T>
): GenericDataSource<T> {

    override suspend fun add(item: T): Boolean = collection.insertOne(item).wasAcknowledged()

    override suspend fun addAndGetId(item: T): String? {
        val newItem = collection.save(item)
        val id = when(val upsert = newItem?.upsertedId) {
            is BsonObjectId -> upsert.value.toHexString()
            else -> upsert.toString()
        }
        return id
    }

    override suspend fun delete(property: KProperty1<T, ObjectId>, value: String): Boolean {
        return try {
            collection.deleteOne(property eq ObjectId(value)).wasAcknowledged()
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getAll(): List<T> {
        return try {
            collection.find().toList()
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    override suspend fun getById(property: KProperty1<T, ObjectId>, value: ObjectId): T? {
        return try {
            collection.findOne(property eq value)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override suspend fun getListByPropertyString(property: KProperty1<T, String>, value: String): List<T> =
        collection.find(property eq value).toList()
}