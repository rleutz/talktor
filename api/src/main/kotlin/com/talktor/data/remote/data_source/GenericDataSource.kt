package com.talktor.data.remote.data_source

import org.bson.types.ObjectId
import kotlin.reflect.KProperty1

interface GenericDataSource<T: Any> {
    suspend fun add(item: T): Boolean
    suspend fun addAndGetId(item: T): String?
    suspend fun delete(property: KProperty1<T, ObjectId>, value: String): Boolean
    suspend fun getAll(): List<T>
    suspend fun getById(property: KProperty1<T, ObjectId>, value: ObjectId): T?
    suspend fun getListByPropertyString(property: KProperty1<T, String>, value: String): List<T>
}