package com.talktor.data.remote.api

import org.litote.kmongo.coroutine.CoroutineDatabase
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

class ClientImpl : Client {

    companion object {
        private const val SERVER = "mongodb://root:senha123@localhost:27017/"
        private const val DATABASE = "talktor"
    }

    private val client = KMongo.createClient(
        connectionString = SERVER
    )
    override val database: CoroutineDatabase = client.coroutine.getDatabase(DATABASE)
}