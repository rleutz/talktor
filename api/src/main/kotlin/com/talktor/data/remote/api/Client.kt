package com.talktor.data.remote.api

import org.litote.kmongo.coroutine.CoroutineDatabase

interface Client {
    val database: CoroutineDatabase
}