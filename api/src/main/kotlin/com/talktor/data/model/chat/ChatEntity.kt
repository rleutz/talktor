package com.talktor.data.model.chat

import com.talktor.domain.model.Chat
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

data class ChatEntity(
    @BsonId val id: ObjectId = ObjectId(),
    val date: Long,
    val userName: String,
    val chatRoomId: String,
    val message: String
) {
    companion object {
        fun fromChat(chat: Chat) = ChatEntity(
            id = if (chat.id.isNullOrBlank()) ObjectId() else ObjectId(chat.id),
            date = chat.date,
            userName = chat.userName,
            chatRoomId = chat.chatRoomId,
            message = chat.message
        )
    }

    fun toChat() = Chat(
        id = id.toString(),
        date = date,
        userName = userName,
        chatRoomId = chatRoomId,
        message = message
    )
}
