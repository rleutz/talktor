package com.talktor.data.model.chat

import com.talktor.domain.model.ChatRoom
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

data class ChatRoomEntity(
    @BsonId val id: ObjectId = ObjectId(),
    val name: String
) {
    companion object {
        fun fromChatRoom(chatRoom: ChatRoom) = ChatRoomEntity(
            id = if(chatRoom.id.isNullOrBlank()) ObjectId() else ObjectId(chatRoom.id),
            name = chatRoom.name
        )
    }

    fun toChatRoom() = ChatRoom(
        id = id.toString(),
        name = name
    )
}
