package com.talktor.presentation.route.chat

import com.talktor.domain.core.DataType
import com.talktor.domain.core.SocketResourceDTO
import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.domain.use_case.web_socket.WebSocketUseCases
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import io.ktor.websocket.*
import kotlinx.coroutines.delay
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.koin.ktor.ext.inject
import java.util.*

fun Route.chatRoutes() {
    val connections = Collections.synchronizedSet<WSConnection>(LinkedHashSet())
    val useCases by inject<ChatUseCases>()
    val chatRoomUseCases by inject<ChatRoomUseCases>()
    val webSocketUseCases by inject<WebSocketUseCases>()
    get("chat") {
        call.respond(HttpStatusCode.OK, chatRoomUseCases.getAll.invoke())
    }
    get("/info/{chatRoomName}") {
        val chatRoomName = call.parameters["chatRoomName"] ?: kotlin.run {
            call.respond(HttpStatusCode.BadRequest)
            return@get
        }
        delay(500)
        val chatRoom = chatRoomUseCases.getByName.invoke(chatRoomName) ?: kotlin.run {
            call.respond(HttpStatusCode.Conflict)
            return@get
        }
        call.respond(HttpStatusCode.OK, chatRoom)
    }
    get("/users/{chatRoomName}") {
        val chatRoomName = call.parameters["chatRoomName"] ?: kotlin.run {
            call.respond(HttpStatusCode.BadRequest)
            return@get
        }
        val chatRoomId = chatRoomUseCases.getIdByName.invoke(chatRoomName) ?: kotlin.run {
            call.respond(HttpStatusCode.NotImplemented)
            return@get
        }
        call.respond(
            HttpStatusCode.OK,
            connections.filter { it.chatRoomId == chatRoomId }.map { it.userName }.toList()
        )
    }
    get("/chat/{chatRoomName}") {
        val chatRoomName = call.parameters["chatRoomName"] ?: kotlin.run {
            call.respond(HttpStatusCode.BadRequest)
            return@get
        }
        val chatRoomId = chatRoomUseCases.getIdByName.invoke(chatRoomName) ?: kotlin.run {
            call.respond(HttpStatusCode.NotImplemented)
            return@get
        }
        call.respond(HttpStatusCode.OK, useCases.getAllByChatRoomId.invoke(chatRoomId))
    }
    webSocket("/ws/{userName}") {
        val userName = call.parameters["userName"] ?: kotlin.run {
            close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "Username not valid."))
            return@webSocket
        }
        if (connections.map { it.userName.lowercase() }.contains(userName.lowercase())) {
            send("User already exist!")
            close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "User already exist!"))
            return@webSocket
        }
        val connection = WSConnection(this, userName)
        connections += connection
        try {
            for (frame in incoming) {
                frame as? Frame.Text ?: continue
                val json = Json.decodeFromString<SocketResourceDTO>(frame.readText())
                val result = webSocketUseCases.receive.invoke(connections, connection, json) ?: kotlin.run {
                    connection.session.close()
                    return@webSocket
                }
                if (result.dataType == DataType.CHAT ||
                    result.dataType == DataType.CHAT_INFO
                ) {
                    connections.filter { it.chatRoomId == connection.chatRoomId }.forEach {
                        it.session.send(Json.encodeToString(result))
                    }
                } else {
                    connection.session.send(Json.encodeToString(result))
                }
            }
        } catch (e: Exception) {
            println(e.toString())
        } finally {
            println("Removing $connection!")
            connections -= connection
        }
    }
}