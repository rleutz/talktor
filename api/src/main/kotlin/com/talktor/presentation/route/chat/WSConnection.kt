package com.talktor.presentation.route.chat

import io.ktor.websocket.*
import org.koin.core.component.KoinComponent

class WSConnection(
    val session: DefaultWebSocketSession,
    val userName: String,
): KoinComponent {

    var chatRoomId: String? = null
}