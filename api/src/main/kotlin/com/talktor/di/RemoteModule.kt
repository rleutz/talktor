package com.talktor.di

import com.talktor.data.remote.api.Client
import com.talktor.data.remote.api.ClientImpl
import com.talktor.data.repository.chat.ChatRepository
import com.talktor.data.repository.chat.ChatRepositoryImpl
import com.talktor.data.repository.chat.ChatRoomRepository
import com.talktor.data.repository.chat.ChatRoomRepositoryImpl
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module


val remoteModule = module {
    singleOf(::ClientImpl) bind Client::class

    factoryOf(::ChatRoomRepositoryImpl) bind ChatRoomRepository::class
    factoryOf(::ChatRepositoryImpl) bind ChatRepository::class

}