package com.talktor.di

import com.talktor.domain.use_case.chat.ChatAddMessageUseCase
import com.talktor.domain.use_case.chat.ChatGetAllByChatRoomIdUseCase
import com.talktor.domain.use_case.chat.ChatJoinUseCase
import com.talktor.domain.use_case.chat.ChatUseCases
import com.talktor.domain.use_case.chat_room.ChatRoomGetAllUseCase
import com.talktor.domain.use_case.chat_room.ChatRoomGetByNameUseCase
import com.talktor.domain.use_case.chat_room.ChatRoomGetIdByNameUseCase
import com.talktor.domain.use_case.chat_room.ChatRoomUseCases
import com.talktor.domain.use_case.web_socket.WebSocketReceiveUseCase
import com.talktor.domain.use_case.web_socket.WebSocketUseCases
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.module

val domainModule = module {
    factoryOf(::ChatRoomGetAllUseCase)
    factoryOf(::ChatRoomGetByNameUseCase)
    factoryOf(::ChatRoomGetIdByNameUseCase)
    factoryOf(::ChatRoomUseCases)

    factoryOf(::ChatAddMessageUseCase)
    factoryOf(::ChatGetAllByChatRoomIdUseCase)
    factoryOf(::ChatJoinUseCase)
    factoryOf(::ChatUseCases)

    factoryOf(::WebSocketReceiveUseCase)
    factoryOf(::WebSocketUseCases)
}