package com.talktor.plugins

import com.talktor.di.domainModule
import com.talktor.di.remoteModule
import io.ktor.server.application.*
import org.koin.ktor.plugin.Koin
import org.koin.logger.slf4jLogger

fun Application.configureKoin() {
    install(Koin) {
        slf4jLogger()
        modules(
            remoteModule,
            domainModule
        )
    }
}