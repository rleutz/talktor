package com.talktor.data.repositories.network.requests

import kotlinx.serialization.Serializable

@Serializable
class ChatRoomResponse(
    val id: String,
    val name: String
)