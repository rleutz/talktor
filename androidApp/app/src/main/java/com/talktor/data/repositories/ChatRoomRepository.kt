package com.talktor.data.repositories

import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.data.repositories.network.requests.ChatRoomResponse
import com.talktor.models.ChatRoom
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.coroutines.flow.flow

class ChatRoomRepository(
    private val dataSource: GenericDataSource
) {

    fun chatRooms() = flow {
        val chatRooms = dataSource.get("/chat")
            .body<List<ChatRoomResponse>>()
            .map { it.toChatRoom() }
        emit(chatRooms)
    }

}

private fun ChatRoomResponse.toChatRoom() = ChatRoom(
    id = id,
    name = name
)