package com.talktor.data.remote.data_source

import io.ktor.client.statement.HttpResponse
import io.ktor.websocket.WebSocketSession
import kotlinx.coroutines.flow.Flow

interface GenericDataSource {
    var webSocketSession: WebSocketSession
    suspend fun get(endpoint: String): HttpResponse
    suspend fun webSocket(
        chatRoomId: String,
        user: String
    )
}