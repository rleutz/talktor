package com.talktor.data.remote.data_source

import com.talktor.data.remote.api.Api
import io.ktor.client.plugins.websocket.webSocketSession
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpMethod
import io.ktor.websocket.Frame
import io.ktor.websocket.WebSocketSession
import io.ktor.websocket.close
import io.ktor.websocket.readText
import io.ktor.websocket.send
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow

class GenericDataSourceImpl(
    private val api: Api
) : GenericDataSource {

    override lateinit var webSocketSession: WebSocketSession
    companion object {
        private const val SERVER = "192.168.0.198"
        private const val SERVER_WEB = "http://$SERVER:8080"
        private const val PORT = 8080
    }
    override suspend fun get(endpoint: String): HttpResponse {
        return api.client.get(SERVER_WEB+endpoint)
    }
    override suspend fun webSocket(
        chatRoomId: String,
        user: String
    ) {
        webSocketSession = api.client.webSocketSession(
            HttpMethod.Get,
            host = SERVER,
            port = PORT,
            path = "/ws/$chatRoomId/$user"
        )
    }
}
