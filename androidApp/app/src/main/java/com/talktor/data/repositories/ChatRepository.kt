package com.talktor.data.repositories

import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.domain.model.Chat
import io.ktor.client.call.body
import io.ktor.websocket.Frame
import io.ktor.websocket.close
import io.ktor.websocket.readText
import io.ktor.websocket.send
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.serialization.json.Json

class ChatRepository(
    private val dataSource: GenericDataSource
) {

    suspend fun close() = dataSource.webSocketSession.close()

    suspend fun connectWebSocket(chatRoomId: String, user: String) {
        try {
            dataSource.webSocket(chatRoomId, user)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getMessages(): Flow<Chat> {
        return try {
            dataSource.webSocketSession.incoming.receiveAsFlow()
                .filter { it is Frame.Text }
                .map {
                    val message = (it as? Frame.Text)?.readText() ?: ""
                    Json.decodeFromString<Chat>(message)
                }
        } catch (e: Exception) {
            e.printStackTrace()
            flow { }
        }
    }

    suspend fun getOldMessages(chatRoomName: String): List<Chat> {
        return dataSource.get("/chat/$chatRoomName").body<List<Chat>>()
    }

    suspend fun sendMessage(message: String) = dataSource.webSocketSession.send(message)

}
