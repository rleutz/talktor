package com.talktor.feature.chat

import com.talktor.domain.model.Chat

data class ChatUiState(
    val chats: List<Chat> = emptyList(),
    val userMessage: String = "",
    val onUserMessageChange: (String) -> Unit = {},
    val currentUser: String = ""
) {
}