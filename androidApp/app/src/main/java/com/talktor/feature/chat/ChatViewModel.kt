package com.talktor.feature.chat

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.talktor.data.repositories.ChatRepository
import com.talktor.domain.model.Chat
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import kotlin.random.Random


class ChatViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val repository: ChatRepository
) : ViewModel() {

    private val _uiState = MutableStateFlow(ChatUiState())
    val uiState = _uiState.asStateFlow()

    suspend fun getOldMessages(chatRoomName: String): List<Chat> {
        return repository.getOldMessages(chatRoomName)
    }

    suspend fun sendMessage(message: String) {
        repository.sendMessage(message)
    }

    init {
        _uiState.update { currentState ->
            currentState.copy(
                onUserMessageChange = { newMessage ->
                    _uiState.update {
                        it.copy(
                            userMessage = newMessage
                            //TODO receber usuário pelo data store
                            , currentUser = "Alex"
                        )
                    }
                }
            )
        }

        viewModelScope.launch {
            savedStateHandle.get<String>("chat")?.let { chatId ->
                repository.connectWebSocket(chatId, "Alex")
                val oldMessages = getOldMessages(chatId)
                _uiState.update { currentState ->
                    currentState.copy(chats = oldMessages)
                }
                repository.getMessages()
                    .collectLatest { result ->
                        _uiState.update { currentState ->
                            val list = uiState.value.chats.toMutableList()
                            list.add(result)
                            currentState.copy(chats = list)
                        }
                    }
            }
        }
    }

    suspend fun sendMessage() {
        val message = _uiState.value.userMessage
        _uiState.update {
            it.copy(userMessage = "")
        }
        withContext(IO) {
            sendMessage(message = message)
        }
    }

}