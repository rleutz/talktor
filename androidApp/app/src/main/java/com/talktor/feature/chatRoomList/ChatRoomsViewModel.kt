package com.talktor.feature.chatRoomList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.talktor.data.repositories.ChatRoomRepository
import com.talktor.models.ChatRoom
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch


data class ChatRoomsUiState(
    val chatRooms: List<ChatRoom> = emptyList()
)

class ChatRoomsViewModel(
    private val repository: ChatRoomRepository
) : ViewModel() {

    private val _uiState = MutableStateFlow(ChatRoomsUiState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            repository.chatRooms().collectLatest { chatRooms ->
                _uiState.update { currentState ->
                    currentState.copy(chatRooms = chatRooms)
                }
            }
        }
    }

}