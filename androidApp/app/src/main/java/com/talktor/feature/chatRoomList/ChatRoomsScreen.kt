package com.talktor.feature.chatRoomList

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import androidx.compose.ui.unit.dp
import com.talktor.models.ChatRoom
import com.talktor.ui.theme.TalktorTheme
import kotlin.random.Random

@Composable
fun ChatRoomsScreen(
    uiState: ChatRoomsUiState,
    modifier: Modifier = Modifier,
    onChatClick: (String?) -> Unit,
) {
    Column(modifier.fillMaxSize()) {
        LazyColumn(Modifier.weight(1f)) {
            items(uiState.chatRooms) { chatRoom ->
                Box(
                    Modifier
                        .fillMaxWidth()
                        .clickable { onChatClick(chatRoom.name) }
                        .padding(16.dp)
                ) {
                    Text(text = chatRoom.name)
                }
            }
        }
    }
}

@Preview
@Composable
private fun ChatRoomListScreenPreview() {
    TalktorTheme {
        Surface(color = MaterialTheme.colorScheme.background) {
            ChatRoomsScreen(
                uiState = ChatRoomsUiState(
                    List(10) {
                        ChatRoom(
                            id = "${it + 1}",
                            name = LoremIpsum(Random.nextInt(1, 20)).values.first()
                        )
                    }
                ),
                onChatClick = {}
            )
        }
    }
}