package com.talktor.feature.chat

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.datasource.LoremIpsum
import androidx.compose.ui.unit.dp
import com.talktor.domain.model.Chat
import com.talktor.ui.theme.TalktorTheme
import kotlinx.coroutines.launch
import kotlin.random.Random

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChatScreen(
    uiState: ChatUiState,
    modifier: Modifier = Modifier,
    onSendMessageClick: () -> Unit,
) {
    val focusManager = LocalFocusManager.current
    Column(modifier.fillMaxSize()) {
        TopAppBar(title = { Text(text = "Chat") })
        LazyColumn(Modifier.weight(1f)) {
            items(uiState.chats) { chat ->
                val isAuthor = remember {
                    uiState.currentUser == chat.userName
                }
                val messageAlignment = if (isAuthor) {
                    Alignment.CenterEnd
                } else {
                    Alignment.CenterStart
                }
                BoxWithConstraints(
                    Modifier
                        .fillMaxWidth()
                ) {
                    Box(
                        modifier = Modifier
                            .padding(8.dp)
                            .widthIn(
                                max = maxWidth / 1.3f
                            )
                            .align(messageAlignment)
                            .background(
                                if (isAuthor) {
                                    Color.Gray.copy(alpha = 0.5f)
                                } else {
                                    Color.Cyan.copy(alpha = 0.5f)
                                },
                                shape = RoundedCornerShape(8.dp)
                            )
                            .padding(8.dp)
                    ) {
                        Text(text = chat.message)
                    }
                }
            }
        }
        TextField(
            value = uiState.userMessage,
            onValueChange = uiState.onUserMessageChange,
            Modifier.fillMaxWidth(),
            placeholder = {
                Text(text = "Mensagem...")
            },
            trailingIcon = {
                Icon(
                    Icons.AutoMirrored.Filled.Send, contentDescription = null,
                    Modifier
                        .clip(
                            CircleShape
                        )
                        .clickable {
                            focusManager.clearFocus()
                            onSendMessageClick()
                        }
                        .padding(8.dp)
                )
            }
        )
    }
}

@Preview
@Composable
private fun ChatScreenPreview() {
    TalktorTheme {
        Surface(color = MaterialTheme.colorScheme.background) {
            ChatScreen(
                uiState = ChatUiState(
                    chats = listOf(
                        Chat(
                            userName = "Alex",
                            chatRoomId = "teste",
                            message = LoremIpsum(Random.nextInt(1, 20))
                                .values.first()
                        ),
                        Chat(
                            userName = "Alex",
                            chatRoomId = "teste",
                            message = LoremIpsum(Random.nextInt(1, 20))
                                .values.first()
                        ),
                    )
                ),
                onSendMessageClick = {}
            )
        }
    }
}