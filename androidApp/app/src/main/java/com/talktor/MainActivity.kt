package com.talktor

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.lifecycle.ViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.talktor.feature.chatRoomList.ChatRoomsScreen
import com.talktor.feature.chat.ChatScreen
import com.talktor.feature.chat.ChatUiState
import com.talktor.feature.chat.ChatViewModel
import com.talktor.feature.chatRoomList.ChatRoomsUiState
import com.talktor.feature.chatRoomList.ChatRoomsViewModel
import com.talktor.ui.theme.TalktorTheme
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TalktorTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    val scope = rememberCoroutineScope()
                    NavHost(navController = navController, startDestination = "chatRoom") {
                        composable("chatRoom") {
                            val viewModel = koinViewModel<ChatRoomsViewModel>()
                            val uiState by viewModel.uiState.collectAsState(initial = ChatRoomsUiState())
                            ChatRoomsScreen(
                                uiState = uiState,
                                onChatClick = {
                                    navController.navigate("chat/$it")
                                }
                            )
                        }
                        composable("chat/{chat}") { backStackEntry ->
                            backStackEntry.arguments?.getString("chat")?.let { chat ->
                                val viewModel = koinViewModel<ChatViewModel>()
                                val uiState by viewModel.uiState.collectAsState(initial = ChatUiState())
                                ChatScreen(uiState, onSendMessageClick = {
                                    scope.launch {
                                        viewModel.sendMessage()
                                    }
                                })
                            } ?: LaunchedEffect(null) {
                                navController.navigateUp()
                            }
                        }
                    }
                }
            }
        }
    }
}