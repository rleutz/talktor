package com.talktor

import android.app.Application
import com.talktor.di.appModule
import com.talktor.di.dataModule
import com.talktor.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TalktorApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(
                appModule,
                dataModule,
                networkModule
            )
            androidContext(this@TalktorApplication)
            androidLogger()
        }
    }

}