package com.talktor.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class Chat(
    val id: String? = null,
    val date: Long = System.currentTimeMillis(),
    val userName: String,
    val chatRoomId: String,
    val message: String
)