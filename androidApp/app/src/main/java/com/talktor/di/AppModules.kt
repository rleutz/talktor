package com.talktor.di

import com.talktor.data.remote.api.Api
import com.talktor.data.remote.api.ApiImpl
import com.talktor.data.remote.data_source.GenericDataSource
import com.talktor.data.remote.data_source.GenericDataSourceImpl
import com.talktor.data.repositories.ChatRepository
import com.talktor.data.repositories.ChatRoomRepository
import com.talktor.feature.chat.ChatViewModel
import com.talktor.feature.chatRoomList.ChatRoomsViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module

val appModule = module {
    viewModelOf(::ChatRoomsViewModel)
    viewModelOf(::ChatViewModel)
}

val dataModule = module {
    factoryOf(::ChatRoomRepository)
    factoryOf(::ChatRepository)
}

val networkModule = module {
    singleOf(::ApiImpl) bind Api::class
    singleOf(::GenericDataSourceImpl) bind GenericDataSource::class
}