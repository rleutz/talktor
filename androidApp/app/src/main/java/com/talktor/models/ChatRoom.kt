package com.talktor.models

data class ChatRoom(
    val id: String? = null,
    val name: String
)
